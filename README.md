# Docker + Node.js

Dockerize a Node.js app. 

Watch the full [Docker video](https://youtu.be/gAkwW2tuIqE) on YouTube or read the [Docker Tutorial](https://fireship.io/lessons/docker-basics-tutorial-nodejs/) on Fireship.io. 

# more to play
docker run -d \
  -it \
  --name devtest \
  -v "$(pwd)"/target:/app \
  nginx:latest <!-- build the ally:latest image yass queen-->

# tips

- 1 process per container
